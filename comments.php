
<?php require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>

<?php Confirm_Login(); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Comments</title>

         <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="css/adminstyles.css">




    </head>
    <body>

        <div style="height: 10px; background: #27aae1;"></div>
        <nav class="navbar navbar-inverse" role="navigation">

            <div class="container">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>


                    </button>
                 <a class="navbar-brand" href="blog.php">   
                    <img style="margin-top: -15px;" src="images/Capture7.PNG" width=80; height=50>
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="collapse">

                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="blog.php" target="_blank">Blog</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Features</a></li>
                </ul>


                <form action="blog.php" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="Search">
                    </div>
                    <button class="btn btn-default" name="SearchButton">Go</button>
                </form>
                </div>



            </div>
            
        </nav>

            <div class="Line" style="height: 10px; background: #27aae1;"></div>


        <div class="container-fluid">
            <div class="row">
                
                <div class="col-sm-2">
<br></br>

                    <ul id="Side-Menu" class="nav nav-pills nav-stacked">
                        <li ><a href="Dashboard.php"><span class="glyphicon glyphicon-th"></span>&nbsp;Dashboard</a></li>
                         <li><a href="AddNewPost.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Add new Post</a></li>
 <li><a href="categories.php"><span class="glyphicon glyphicon-tags"></span>&nbsp;Categories</a></li>
 <li><a href="admins.php"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Admin</a></li>
 <li class="active"><a href="comments.php"><span class="glyphicon glyphicon-comment"></span>&nbsp;Comments</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-equalizer"></span>&nbsp;Live Blog</a></li>
 <li><a href="Logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
`

                    </ul>
                </div>




                <!-- Ending of side area -->

                <div class="col-sm-10"> <!-- Main area -->

                      <div><?php echo Message(); 
                        echo SuccessMessage();

                        ?></div>
                   <h1>Un-Approved Comments</h1>
                   <div class="table-responsive">
                       <table class="table table-striped table-hover">
                           <tr>
                               <th>No.</th>
                               <th>Name</th>
                               <th>Date</th>
                               <th>Comment</th>
                               <th>Approve</th>
                               <th>Delete Comments</th>
                               <th>Details</th>

                           </tr>

                           <?php
                           global $conn;
                    $Query="SELECT * FROM comments WHERE status='OFF' ORDER BY id desc";
                    $Execute=mysqli_query($conn,$Query);
                    $SrNo=0;
                    while ($DataRows=mysqli_fetch_array($Execute)) {
                        $CommentId=$DataRows["id"];
                        $DateTimeOfComment=$DataRows["datetime"];
                        $PersonalName=$DataRows["name"];
                        $PersonalComment=$DataRows["comment"];
                        $CommentedPostId=$DataRows["admin_panel_id"];
                        $SrNo++;

                    


                            ?>
                            <tr>
                                <td><?php echo htmlentities($SrNo); ?></td>
                                <td><?php echo htmlentities($PersonalName); ?></td>
                                <td><?php echo htmlentities($DateTimeOfComment); ?></td>
                                <td><?php echo htmlentities($PersonalComment); ?></td>
                                <td><a href="ApproveComments.php?id=<?php echo $CommentId;?>"><span class="btn btn-success">Approve</span></a></td>
                                <td><a href="deleteComments.php?id=<?php echo $CommentId;?>"><span class="btn btn-danger">Delete</span></a></td>
                                <td><a href="FullPost.php?id=<?php echo $CommentedPostId; ?>" target="_blank"><span class="btn btn-primary">Live Preview</span></a></td>



                            </tr>
                        <?php } ?>


                       </table>
                   </div>




                   <h1>Approved Comments</h1>
                   <div class="table-responsive">
                       <table class="table table-striped table-hover">
                           <tr>
                               <th>No.</th>
                               <th>Name</th>
                               <th>Date</th>
                               <th>Comment</th>
                               <th>Approved By</th>
                               <th>Revert Approve</th>
                               <th>Delete Comments</th>
                               <th>Details</th>

                           </tr>

                           <?php
                           global $conn;
                           
                    $Query="SELECT * FROM comments WHERE status='ON' ORDER BY id desc";
                    $Execute=mysqli_query($conn,$Query);
                    $SrNo=0;
                    while ($DataRows=mysqli_fetch_array($Execute)) {
                        $CommentId=$DataRows["id"];
                        $DateTimeOfComment=$DataRows["datetime"];
                        $PersonalName=$DataRows["name"];
                        $PersonalComment=$DataRows["comment"];
                        $ApprovedBy=$DataRows["approvedby"];
                        $CommentedPostId=$DataRows["admin_panel_id"];
                        $SrNo++;

                     


                            ?>
                            <tr>
                                <td><?php echo htmlentities($SrNo); ?></td>
                                <td><?php echo htmlentities($PersonalName); ?></td>
                                <td><?php echo htmlentities($DateTimeOfComment); ?></td>
                                <td><?php
                                if(strlen($PersonalComment)>15){$PersonalComment=substr($PersonalComment,0,15).'....';}

                                 echo htmlentities($PersonalComment); ?></td>
                                 <td><?php echo htmlentities($ApprovedBy); ?></td>

                                <td><a href="DisApproveComments.php?id=<?php echo $CommentId; ?>"><span class="btn btn-warning">Dis-Approve</span></a></td>
                                 <td><a href="deleteComments.php?id=<?php echo $CommentId; ?>"><span class="btn btn-danger">Delete</span></a></td>
                                  <td><a href="FullPost.php?id=<?php echo $CommentedPostId; ?>" target="_blank"><span class="btn btn-primary">Live Preview</span></a></td>




                            </tr>
                        <?php } ?>


                       </table>
                   </div>
                   
                      


                   
                </div>

                <!-- Ending of main area -->
            </div>

            <!-- Ending of row-->
        </div>
            <!-- Ending of container-->

<div id="footer">
    <hr>
    <p>Theme by | Gikundiro koloni | &copy;2019-2020 ---- Allright reserved.</p>
    <a style="color: white; text-decoration: none; cursor: pointer; font-weight: bold;" href="#">
        
<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

    </a>
   
</div>




    </body>
    </html>