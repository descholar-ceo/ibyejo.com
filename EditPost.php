<?php 
require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>

<?php Confirm_Login(); ?>

<?php 
if (isset($_POST["Submit"])) {
    $Title=mysqli_real_escape_string($conn, $_POST["Title"]);
    $Category=mysqli_real_escape_string($conn, $_POST["Category"]);
    $Post=mysqli_real_escape_string($conn, $_POST["Post"]);
    date_default_timezone_set("Africa/Kigali");
$CurrentTime=time();
$DateTime=strftime("%d-%B-%Y %H:%M:%S",$CurrentTime);
$DateTime;
$Admin=$_SESSION["Username"];
$Image=$_FILES["Image"]["name"];
$Target="Upload/".basename($_FILES["Image"]["name"]);
if(empty($Title)) {
    $_SESSION["ErrorMessage"]="Title can not be empty";
    Redirect_to("AddNewPost.php");

}elseif(strlen($Title)<5) {
    $_SESSION["ErrorMessage"]="Title should be atleast 5 characters";
    Redirect_to("AddNewPost.php");

}elseif(empty($Post)) {
    $_SESSION["ErrorMessage"]="Category can not be empty";
    Redirect_to("AddNewPost.php");

}else{

    global $conn;
    $EditFromUrl=$_GET['Edit'];
     $Query="UPDATE admin_panel SET datetime='$DateTime', title='$Title', category='$Category', author='$Admin', image='$Image', post='$Post' WHERE id='$EditFromUrl'";

    $Execute=mysqli_query($conn,$Query);
    move_uploaded_file($_FILES["Image"]["tmp_name"], $Target);


    if ($Execute) {
         $_SESSION["SuccessMessage"]="Post Updated successfully";
    Redirect_to("Dashboard.php");
         
    }else{
         $_SESSION["ErrorMessage"]="Something went wrong try again";
    Redirect_to("Dashboard.php");
    }

}

}

?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Edit Post</title>

         <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="css/adminstyles.css">


                  <style>
                .FieldInfo{
                    color: rgb(251, 174, 44);
                    font-family: Bitter,Georgia,"Times New Roman",Times,serif;
                    font-size: 1.2em;
                    

                </style>




    </head>
    <body>


        <div class="container-fluid">
            <div class="row">
                
                <div class="col-sm-2">


                    <ul id="Side-Menu" class="nav nav-pills nav-stacked">
                        <li><a href="Dashboard.php"><span class="glyphicon glyphicon-th"></span>&nbsp;Dashboard</a></li>
                         <li   class="active"><a href="AddNewPost.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Add new Post</a></li>
 <li><a href="categories.php"><span class="glyphicon glyphicon-tags"></span>&nbsp;Categories</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Admin</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-comment"></span>&nbsp;Comments</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-equalizer"></span>&nbsp;Live Blog</a></li>
 <li><a href="Logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
`

                    </ul>
                </div>




                <!-- Ending of side area -->

                <div class="col-sm-10"> 
                   <h1>Update Post</h1>
                   <?php echo Message(); 
                         echo SuccessMessage();

                        ?>

                    <div>

                        <?php 
                        $SearchQueryParameter=$_GET['Edit'];
                        $conn;
                        $Query="SELECT * FROM admin_panel WHERE id='$SearchQueryParameter'";
                         $ExecuteQuery=mysqli_query($conn,$Query);
                    while ($DataRows=mysqli_fetch_array($ExecuteQuery)) {
                        $TitleToBeUpdated=$DataRows["title"];
                        $CategoryToBeUpdated=$DataRows["category"];
                        $ImageToBeUpdated=$DataRows["image"];
                        $PostToBeUpdated=$DataRows["post"];
                    }




                        ?>
                        
                        <form action="EditPost.php?Edit=<?php echo  $SearchQueryParameter; ?>" method="post" enctype="multipart/form-data">

                            <fieldset> 
                                <div class="form-group">
                                <label for="title"><span class="FieldInfo">Title:</span></label>

                                <input value="<?php echo $TitleToBeUpdated; ?>" class="form-control" type="text" name="Title" id="title" placeholder="Title">

                                </div>

                                 <div class="form-group">
                                    <span class="FieldInfo">Existing category:</span>
                                    <?php echo $CategoryToBeUpdated; ?>
                                    <br></br>
                                <label for="categoryselect"><span class="FieldInfo">Category:</span></label>

                                <select class="form-control" id="categoryselect" name="Category">
                                    
                                     <?php 
                    global $conn;
                    $ViewQuery="SELECT * FROM category ORDER BY datetime desc";
                    $Execute=mysqli_query($conn,$ViewQuery);
                    while ($DataRows=mysqli_fetch_array($Execute)) {
                        $Id=$DataRows["id"];
                        $CategoryName=$DataRows["name"];
                        
                    
                     ?>

                     <option><?php echo $CategoryName; ?></option>

                 <?php } ?>

                                </select>
                                </div>

                                 <div class="form-group">
                                      <span class="FieldInfo">Existing Image:</span>
                                    <img src="Upload/<?php echo $ImageToBeUpdated; ?>"width="160px" height="50px;">
                                    <br></br>
                                <label for="imageselect"><span class="FieldInfo">Select Image:</span></label>

                                <input type="File" class="form-control" name="Image" id="imageselect">
                            </div>


                              <div class="form-group">
                                <label for="postarea"><span class="FieldInfo">Post:</span></label>

                                <textarea class="form-control" name="Post" id="postarea">
                                    
                                    <?php echo  $PostToBeUpdated; ?>
                                </textarea>

                            </div>

                                <br>
                            


                                <input class="btn btn-success btn-block" type="submit" name="Submit" value="Update Post">
                            </fieldset>   
                            </br>                         


                        </form>
                    </div> 


                
                   
                    
            </div>

                <!-- Ending of main area -->
            </div>

            <!-- Ending of row-->
        </div>
            <!-- Ending of container-->

<div id="footer">
    <hr>
    <p>Theme by | Gikundiro koloni | &copy;2019-2020 ---- Allright reserved.</p>
    <a style="color: white; text-decoration: none; cursor: pointer; font-weight: bold;" href="#">
        
<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

    </a>
   
</div>




    </body>
    </html>

   