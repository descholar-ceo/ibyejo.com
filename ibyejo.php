<?php require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("content__security.php"); ?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ibyejo</title>
    <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">



  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">



  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
 
    <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/my-blog-design.css">
  <link rel="stylesheet" href="css/responsive.css">

  <link rel="stylesheet" href="css/blogstyle.css">


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


  <style>
    * {
      box-sizing: border-box;
    }
    #footer-area{
      padding: 90px 0 60px 0;
      background: #000;
      color: #fff;

    }
    .footer-social{
      margin-top: 20px;

    }
    .single-footer h3{
      font-weight: 500;
      margin-bottom: 25px;
      color: #fafafa;
    }
    .link-area li{
      padding: 5px 5px 5px;
      list-style: none;
    }
    .link-area li a{
      text-transform: capitalize;
      color: #fff;
    }

    .link-area li a i{
      margin-right: 10px;
      color: #e40c78;
    }
    .link-area{
      padding: 0;
    }
    .single-footer p{
    }
    .footer-social li a{
      width: 30px;
      height: 30px;
      display: inline-block;
      background: #000;

    }

    .footer-social li a i{
      color: #e40c78;
      padding: 8px;
    }
    .widget li{
      float: left;
      width: 50%;
    }
    .copyright-area{
      background: #262626;
      padding: 10px 0;
      margin-top: 30px;
      border-radius: 5px;
      margin-bottom: -59.9px;
      border-right: none;border-left: none;-moz-border-radius: 0px;
      -webkit-border-radius: 0px;
      border-radius: 0px;

    }
    .copyright-area p{
      font-weight: 600;
      color: #e40c78;
      margin-top: 20px;
    }
    .copyright-header{
      background: #000;
      padding: 30px 0;
      margin-top: 1px;
      border-radius: 5px;
    }
    .copyright-header p{
      font-weight: bold;
      color: #e40c78;
      font-size: 80px;
    }
    .search-box{
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%,-50%);
      height: 40px;
      border-radius: 40px;
      padding: 10px;
      width: 18em;
      margin-left: -152px;
    }

    .search-box:hover > .search-text{
      width: 240px;
      padding: 0 6px;
    }


    .search-btn{
      color: #e40c78;
      float: right;
      width: 40px;
      height: 40px;
      border-radius: 50%;
      background: #2f3640;
      display: flex;
      justify-content: center;
      align-items: center;
      margin-top: 8px;
    }

    .search-text{
      border:none;
      background: none;
      outline: none;
      float: left;
      padding: 0;
      color: white;
      font-size: 16px;
      transition: 0.4s;
      line-height: 40px;
      margin-top: 8px;
      width: 0px;
      background: #2f3640;
      border-radius: 10px;
      transition: 0.4s;
    }
    #header{
      margin-left: 10px;
      margin-top: -8px;
      margin-left: 10px;
      font-size: 15px;
    }
    #head{
    }
    .main{
      width: 100%;
      background-color: transparent;
      height: 100%;
      margin-left: 5px;
      margin-top: 2px;
    }
    .img-resp{
      height: 384.5px;
      position: absolute;
      margin-top: -20px;
      left: 16px;
      width: 85.4%;

    }
    .description{
      font-weight: bold;
      font-size: 12px;
    }
    .thumbnail{
      background-color: #ffffff;
      width: 80%;
      height: 10em;
      border-right: none;
      border-bottom: none;
      margin-top: 0px;
      -moz-border-radius: 0px;
      -webkit-border-radius: 0px;
      border-radius: 0px;
    }
    .blogposts{
      background-color: #ffffff;
      width: 80%;
      height: 10em;
      border-right: none;
      border-bottom: none;
      margin-top: 0px;
      -moz-border-radius: 0px;
      -webkit-border-radius: 0px;
      border-radius: 0px;
    }
    .big-post{
      background-color: #000000;
      width: 88.8%;
      height: 39.5em;
    }
    .img-respo{
      height: 10.8%;
      position: absolute;
      margin-top: -20px;
      left: 15px;
      width: 85.3%;
    }
    .img-resp{
      width: 85.4%;
      margin-left: -2px;
    }

    .main-inf{
      background-color: #000000;
      width: 103.6%;
      margin-left: -18px;
      height: 125.3%
    }
    .para-first{

     color: #ffffff;
     margin-left: 10px;
     font-size: 17px;
   }
   .sec-bl-po{
    width:50%;
    background-color: #000000;
    height: 145px;
    left: -8px;
  }
  .thi-bl-po{
    width:50%;
    background-color: #000000;
    height: 145px;
    left: -8px;
    margin-top: 2px;
  }
  .for-bl-po{
    width:50%;
    background-color: #000000;
    height: 145px;
    left: -8px;
    margin-top: 2px;
  }
  .second-im-lo{
    height: 145px;
    width: 35.7%;
    margin-left: 6px;
  }
  .sec-para-l{
    margin-left: 195px;
    color: #ffffff;
    font-size: 17px;
    width: 75.8%;
  }
  .sec-dt-l{
    margin-left: 195px;
    color: #e40c78;
    margin-top: -8px;
  }
  .for-dt-l{
    margin-left: 193px;
    color: #e40c78;
    margin-top: -8px;
  }
  .for-para-l{
    margin-left: 193px;
    color: #ffffff;
    font-size: 17px;
    width: 54.2%;
  }
  .f-color{
    stop-color:rgb(255,95,0);
    stop-opacity:1;
  }
  .s-color{
    stop-color:rgb(225,0,255);
    stop-opacity:1;
  }

  .h-img-view{
    height: 278px;
    object-fit: cover;
    margin-left: -2px;
    width: 99.9%;
    margin-right: 3px;
  }

  .main-blog-area{
    background-color: white;
    width: 99%;
  }
  
  .main-img{
    height: 278px;
    object-fit: cover;
    margin-left: -2px;
    width: 99.9%;
    margin-right: 3px;
  }


  @media only screen and (max-width: 360px){   
    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }
    .blog_main-post{
      width: 100%;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%;
    }
  form{
    display: none;
  }

}


  @media only screen and (max-width: 360px) and (min-width: 359px) {

    form{
      display: none;
    }

    .container .mainpost, .container article, .container .card{

    }
    .container .card{
      width: 100%;
    }

    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      min-height: 280px;
    }
    .main-blog-area .card img{
      width: 100%;
      float: left;
      max-height: 390px;
    }
    .main-blog-area .card p{
      margin-top: 50px;
    }
    .main-blog-area .card h2{
      margin-top: 120px;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 390px;
    }
    .main-blog-area .big-card{
      width: 100%;
      max-height: 20px;

    }
    .main-blog-area .big-card .card-body img{
      width: 100%;
      max-height: 900px;

    }

    .main-blog-area .big-card .card-header{
      margin: 0px;
      padding: 0px;
      float: left;
    }
    .main-blog-area .big-card .card-body img{
      width: 40%;
      max-height: 900px;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }  
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }
    .main-blog-area .main-post .main-post-data h1{
float: right;
  }
}


  @media only screen and (max-width: 375px) {


 form{
    display: none;
  }
    .container .card{
      width: 100%;
    }

    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .card img{
      width: 100%;
      float: left;
    }

    .main-blog-area .big-card{
      width: 100%;
      max-height: 900px;

    }
    .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
      margin: 0px;
      padding: 0px;
    }
    .main-blog-area .big-card .card-body img{
      width: 100%;

    }

    .main-blog-area .big-card .card-header{
      margin: 0px;
      padding: 0px;
      float: left;
    }
    .main-blog-area .big-card .card-body img{
      width: 40%;
    }

    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }

    .blog_main-post{
      width: 100%;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }
    .main-blog-area .main-post .main-post-data h1{
      float: right;
    }

  }

  @media only screen and (max-width: 411px) {

     form{
    display: none;
  }
    .container .card{
      width: 100%;
    }

    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .card img{
      width: 100%;
      float: left;
    }
    .main-blog-area .card p{
      margin-top: 50px;
    }
    .main-blog-area .card h2{
      margin-top: 120px;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .big-card{
      width: 100%;
      max-height: 900px;

    }

    .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
      margin: 0px;
      padding: 0px;
    }
    .main-blog-area .big-card .card-body img{
      width: 100%;

    }

    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }

    .blog_main-post{
      width: 100%;
    }

    div .btn_post{
      margin-top: 20px;
      margin-bottom: 8px;
      margin-left: 0px;
      width: 100%;
      float: right;
    }
    .look_more-post{
      text-decoration: none;
      color: #ffffff;
      background-color: #e40c78;
      border-radius: 20px;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 15px;
      padding-bottom: 15px;
      font-size: 15px;
      margin-bottom: 55px;
    }
    .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }
     form{
    display: none;
  }

    
  }


  @media only screen and (max-width: 520px) {
form{
  display: none;
}

}
  @media only screen and (max-width: 760px) {






form{
  margin-left: 80px;
}

section .container{
  width: 100%;
}

    .container .card{
      width: 100%;
    }
    .container article{
      width: 100%;
    }
    .container article img{
      width: 100%;
    }
    .main-blog-area .card{
      width: 100%;
      max-height: 280px;
    }
    .main-blog-area .big-card{
      width: 80%;
      max-height: 900px;

    }
    .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
      margin: 0px;
      padding: 0px;
    }
    .main-blog-area .big-card .card-body img{
      width: 100%;
      
    }

    .blog_main-post .blog_item-post{
      width: 100%;
      float: left;
    }

    .blog_main-post{
      width: 100%;
    }
    .aside-column{
      display: none;
    }
    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }

  }


  @media only screen and (min-width: 415px) and (max-width: 1040px){
    .main-blog-area .card{
      width: 70%;
      max-height: 180px;
    }
    .main-blog-area .card img{
      width: 40%;
      float: left;
    }
    .main-blog-area .big-card{
      width: 80%;
      max-height: 900px;
    }
    .main-blog-area .card{
     border-top: 0.5px solid #2b2d32;
   }
   .main-blog-area .big-card .card-header{
    margin: 0px;
    padding: 0px;
  }
  .main-blog-area .big-card .card-body img{
    width: 40%;
  }
  .main-blog-area .big-card .card-header h2,.main-blog-area .big-card .card-header p{
    margin: 0px;
    padding: 0px;
  }
  .main-blog-area .big-card .card-body img{
    width: 100%; 
  }
  .blog_main-post .blog_item-post{
    width: 100%;
    float: left;
  }
  .blog_main-post{
    width: 80%;
  }
   .aside-column{
      display: none;
    }

    .main-column{
      width: 100%
    }
     .main-blog-area .main-post .main-post-data img{
      float: left;
      width: 50%;
    }

}
  @media only screen and (min-width: 1041px) and (max-width: 1298px){

.col-sm-offset-1{
  margin: 0px;
  padding: 0px;
    margin-top: 9px;
    margin-bottom: 15px;
    width: 81%;
  float: right;
  font-style: italic;
}



}


.main-blog-area .main-post .main-post-data img{
height: 160px;
}
.main-blog-area .main-post .main-post-data h1{
  font-size: 17px;
  text-decoration: none;
  color: black;

}

.main-blog-area .main-post .main-post-data h1:hover {
   text-decoration:none;
   color: gray;
}
.main-blog-area .main-post .main-post-data p{
  color: #e40c78;
  font-weight: normal;
}

.navbar{
  background-color: #000000;moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;
}
a:hover{
color: #e40c78;

}
a{
  color: #e40c78;
}
.post_header:hover{
  color: #e40c78; 
}
.post_header{
  color: #ffffff;
}
.container-fluid{
  background: linear-gradient(to  left,red, #e40c78);
  width: 100%;

}
.card{
  background-color: #000000;
}
ul{
  list-style-type: none
}



</style>

</head>

<body>


  <!--main post image that display on the top of the page-->


 <section id="showcase container-fluid">
  <img class="img-fluid" src="images/office.jpg" style="max-height: 400px;width: 100%;object-fit: cover;">
</section>


<!--Ending of the main post image that display on the top of the page-->


<!--the begining of the main header that contains all buttons that direct you to the different pages-->
<nav class="navbar navbar-expand-lg navbar-light m-0 p-0" style="">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler text-light bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto" style="text-transform: uppercase;  letter-spacing: -1px;font: small-caps bold 13px/30px Georgia, serif;
">
      <li class="nav-item active">
        <a class="nav-link text-light" href="ibyejo">Home</a>
      </li>
       <?php
      global $conn;
      $ViewQuery="SELECT * FROM category ORDER BY id desc";
      $Execute=mysqli_query($conn,$ViewQuery);
      while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows['id'];
        $Category=$DataRows['name'];
        ?>




      <li class="nav-item">
        <a class="nav-link text-light" href="category.php?Category=<?php
        echo $Category; ?>"><?php echo $Category."<br>"; ?></a>
      </li>
     <?php }?>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="search">
      <input class="form-control mr-sm-2" name="Search" type="search" placeholder="Search" aria-label="Search" style="padding: 16px;">
      <button name="SearchButton" class="btn btn-outline-success my-2 my-sm-0" type="submit" style="padding: 8px;border: none;background-color: #e40c78;color: #ffffff;">Search</button>
    </form>
  </div>
</nav>

<!--- Ending of the main header-->

<!------------------------------------------------------------------------------------------------------------------------------->

<!-- Modal content, contains the most 4 first post that posted -->

<div class="container-fluid mt-0 m-0 pt-1 pr-4 pb-2 pl-0 ml-0">

 <ul class=" col-sm-12  m-0 p-0 ml-2 " style="background-color: #000000">
  <div class="main-post col-sm-12 col-md-6 m-0 p-0 pr-1">
  <li>
   <div class="card col-sm-12 m-0 p-0" style="border: none;border: none;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">

     <?php
     global $conn;
     $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,1";
     $Execute=mysqli_query($conn,$ViewQuery);
     while ($DataRows=mysqli_fetch_array($Execute)) {
      $Id=$DataRows["id"];
      $Title=$DataRows["title"];
      $DateTime=$DataRows["datetime"];
      $Image=$DataRows["image"];
      if (strlen($DateTime)>12) {
        $DateTime=substr($DateTime, 0,12);
      }
      ?>
        <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none">

      <div class="card-header col-md-12 col-sm-12 m-0 p-0">
          <img class="img-fluid p-0 m-0" style="width: 100%;height: 100%;object-fit: cover" src="Upload/<?php echo htmlentities($Image) ?>">
        
      </div>
      <div class="card-body col-md-12 col-sm-12">
          <p class="post_header" style="font-size: 18px">
           <?php
           if(strlen($Title)>60){$Title=substr($Title,0,250).'....';}
           echo htmlentities($Title); ?>
         </p>
     <?php } ?> 
   </div>
 </a>
 </div>
</li>
</div>

  <!--- the side bar article contains the post -->

<div class="col-sm-12 col-md-6 p-0 m-0" style="background-color: #000000;min-height: 446px;">
<li>
 <div class="art-post-three col-sm-12 col-md-12 m-0 p-0">
    <?php
    global $conn;
    $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 1,1";
    $Execute=mysqli_query($conn,$ViewQuery);
    while ($DataRows=mysqli_fetch_array($Execute)) {
      $Id=$DataRows["id"];
      $Title=$DataRows["title"];
      $DateTime=$DataRows["datetime"];
      $Image=$DataRows["image"];
      if (strlen($DateTime)>16) {
        $DateTime=substr($DateTime, 0,16);
      }
      ?>
              <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none">

         <div class="col-sm-4 m-0 p-0">
        <img class="img-fluid m-0 p-0" style="width: 100%;height: 100%;object-fit: cover" src="Upload/<?php echo htmlentities($Image) ?>">
      </div>
      <div class="col-sm-12 col-md-7">
        <span class="post_header" style="font-size: 18px">
          <?php
          echo htmlentities($Title); ?>
        </span>
        <p><?php echo htmlentities($DateTime); ?></p>
      </div>
      </a>
    <?php } ?> 
  </div>
</li>

<!----------------------------->

<li>
 <div class="art-post-three col-sm-12 col-md-12 mt-3 m-0 p-0" style="background-color: #000000;">
    <?php
    global $conn;
    $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 2,1";
    $Execute=mysqli_query($conn,$ViewQuery);
    while ($DataRows=mysqli_fetch_array($Execute)) {
      $Id=$DataRows["id"];
      $Title=$DataRows["title"];
      $DateTime=$DataRows["datetime"];
      $Image=$DataRows["image"];
      if (strlen($DateTime)>16) {
        $DateTime=substr($DateTime, 0,16);
      }
      ?>
              <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none">

         <div class="col-sm-4 m-0 p-0">
        <img class="img-fluid m-0 p-0" style="width: 100%;height: 100%;object-fit: cover" src="Upload/<?php echo htmlentities($Image) ?>">
      </div>
      <div class="col-sm-12 col-md-7">
        <span class="post_header" style="font-size: 18px">
          <?php
          echo htmlentities($Title); ?>
        </span>
        <p><?php echo htmlentities($DateTime); ?></p>
      </div>
      </a>
    <?php } ?> 
  </div>
</li>
<!------------------------->
<li>
 <div class="art-post-five col-sm-12 col-md-12 mt-3 m-0 p-0" style="background-color: #000000;">
    <?php
    global $conn;
    $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 3,1";
    $Execute=mysqli_query($conn,$ViewQuery);
    while ($DataRows=mysqli_fetch_array($Execute)) {
      $Id=$DataRows["id"];
      $Title=$DataRows["title"];
      $DateTime=$DataRows["datetime"];
      $Image=$DataRows["image"];
      if (strlen($DateTime)>16) {
        $DateTime=substr($DateTime, 0,16);
      }
      ?>
        <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none">
         <div class="col-sm-4 m-0 p-0">
        <img class="img-fluid m-0 p-0" style="width: 100%;height: 100%;object-fit: cover" src="Upload/<?php echo htmlentities($Image) ?>">
      </div>
      <div class="col-sm-12 col-md-7">
        <span class="post_header" style="font-size: 18px">
          <?php
          echo htmlentities($Title); ?>
        </span>
        <p><?php echo htmlentities($DateTime); ?></p>
      </div>
      </a>
    <?php } ?> 
  </div>
</li>
</div>



</div>

</ul>

</div>



<!--- Ending of the main model content that contains all of the 4 main first post-->


<!-------------------------------------------------------------------------------------------------------------------------------->

<!--- this one is a 5 posts that are on one line and i just do it just to differentiate the design and make it more user friendly and attrection -->


<div class="container-fluid main-blog-area mt-3 ml-1 mr-1" style="background:  #F5F5F5;margin-top: 8px; border-top: 5px solid #e40c78;margin: 2px;"><!---Container--->
  <div class="row">
    <div class="main-blog col-sm-12 col-md-12 mt-2 p-0 m-0 ">
    <div class="main-post col-sm-12 col-md-3 m-0 m-0 pl-1 ">

      
       <?php

         global $conn;

         if (isset($_GET["SearchButton"])) {
           $Search=$_GET["Search"];
           $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

         }
//query when category is active
         elseif (isset($_GET["Category"])) {
          $Category=$_GET["Category"];
          $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
        }
        elseif (isset($_GET["Page"])) {
          $Page=$_GET["Page"];
          if ($Page==0 ||$Page<1) {
            $ShowPostFrom=0;   
          }else{


            $ShowPostFrom=($Page*5)-5;
          }
          $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 4,1";
        }
        else{
         $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
       }

       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
         $PostId=$DataRows["id"];
         $DataTime=$DataRows["datetime"];
         $title=$DataRows["title"];
         $Category=$DataRows["category"];
         $Admin=$DataRows["author"];
         $Image=$DataRows["image"];
         $Post=$DataRows["post"];

         ?>

        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
<div class="row ml-1 mr-1 m-0 list-post">
      <div class="col-sm-6 col-md-12 col-xs-6 p-0 m-0">
         <img class="img-fluid" style="width: 100%;height: 200px;object-fit: cover;" src="Upload/<?php echo  $Image; ?>">
      </div>
      <div class="col-sm-6 col-md-12 col-xs-6 pt-3 m-0 p-0">
        <h1 class="text-dark" style="font-size: 14px"><?php echo htmlentities($title); ?></h1>
                                      <p>

           Published on <?php echo htmlentities($DataTime)  ?>
        
        </p>

         <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p class=" " style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>

          <?php } ?>

      </div>
    </div>
    </a>
        <?php } ?>
    </div>
    <div class="col-sm-12 col-md-3 m-0 pl-1 m-0">

       <?php

         global $conn;

         if (isset($_GET["SearchButton"])) {
           $Search=$_GET["Search"];
           $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

         }
//query when category is active
         elseif (isset($_GET["Category"])) {
          $Category=$_GET["Category"];
          $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
        }
        elseif (isset($_GET["Page"])) {
          $Page=$_GET["Page"];
          if ($Page==0 ||$Page<1) {
            $ShowPostFrom=0;   
          }else{


            $ShowPostFrom=($Page*5)-5;
          }
          $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 5,1";
        }
        else{
         $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
       }

       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
         $PostId=$DataRows["id"];
         $DataTime=$DataRows["datetime"];
         $title=$DataRows["title"];
         $Category=$DataRows["category"];
         $Admin=$DataRows["author"];
         $Image=$DataRows["image"];
         $Post=$DataRows["post"];

         ?>
        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
            <div class="row ml-1 mr-1 m-0">

      <div class="col-sm-6 col-md-12 m-0 p-0">
       <img class="img-fluid" style="width: 100%;height: 200px;object-fit: cover;" src="Upload/<?php echo  $Image; ?>">
      </div>
      <div class="col-sm-6 col-md-12 m-0 pt-3 p-0">
         <h1 class="text-dark" style="font-size: 14px;"><?php echo htmlentities($title); ?></h1>
                                       <p>

           Published on <?php echo htmlentities($DataTime)  ?>
        
        </p>
         <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p class=" " style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>

          <?php } ?>
      </div>
    </div>
    </a>
    <?php } ?>
    </div>
      <div class="col-sm-12 col-md-3 pl-1 m-0">
          <?php

         global $conn;

         if (isset($_GET["SearchButton"])) {
           $Search=$_GET["Search"];
           $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

         }
//query when category is active
         elseif (isset($_GET["Category"])) {
          $Category=$_GET["Category"];
          $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
        }
        elseif (isset($_GET["Page"])) {
          $Page=$_GET["Page"];
          if ($Page==0 ||$Page<1) {
            $ShowPostFrom=0;   
          }else{


            $ShowPostFrom=($Page*5)-5;
          }
          $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 6,1";
        }
        else{
         $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
       }

       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
         $PostId=$DataRows["id"];
         $DataTime=$DataRows["datetime"];
         $title=$DataRows["title"];
         $Category=$DataRows["category"];
         $Admin=$DataRows["author"];
         $Image=$DataRows["image"];
         $Post=$DataRows["post"];

         ?>
        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
          <div class="row ml-1 mr-1 m-0">

      <div class="col-sm-6 col-md-12 m-0 p-0">
        <img class="img-fluid" style="width: 100%;height: 200px;object-fit: cover;" src="Upload/<?php echo  $Image; ?>">

      </div>

      <div class="col-sm-6 col-md-12 m-0 pt-3 p-0">
        <h1 class="text-dark" style="font-size: 14px"><?php echo htmlentities($title); ?></h1>
                        <p>

           Published on <?php echo htmlentities($DataTime)  ?>
        
        </p>
         <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>

          <?php } ?>
      </div>
    </div>
    </a>
    <?php } ?>
    </div>

       <div class="col-sm-12 col-md-3 pl-1 m-0">
          <?php

         global $conn;

         if (isset($_GET["SearchButton"])) {
           $Search=$_GET["Search"];
           $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

         }
//query when category is active
         elseif (isset($_GET["Category"])) {
          $Category=$_GET["Category"];
          $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
        }
        elseif (isset($_GET["Page"])) {
          $Page=$_GET["Page"];
          if ($Page==0 ||$Page<1) {
            $ShowPostFrom=0;   
          }else{


            $ShowPostFrom=($Page*5)-5;
          }
          $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 7,1";
        }
        else{
         $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
       }

       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
         $PostId=$DataRows["id"];
         $DataTime=$DataRows["datetime"];
         $title=$DataRows["title"];
         $Category=$DataRows["category"];
         $Admin=$DataRows["author"];
         $Image=$DataRows["image"];
         $Post=$DataRows["post"];

         ?>
        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
      <div class="row ml-1 mr-1 m-0">

      <div class="col-sm-6 col-md-12 m-0 p-0">
        <img class="img-fluid" style="width: 100%;height: 200px;object-fit: cover;" src="Upload/<?php echo  $Image; ?>">

      </div>

      <div class="col-sm-6 col-md-12 m-0 pt-3 p-0">
        <h1 class="text-dark" style="font-size: 14px"><?php echo htmlentities($title); ?></h1>
                        <p>

           Published on <?php echo htmlentities($DataTime)  ?>
        
        </p>
         <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>

          <?php } ?>
      </div>
    </div>
    </a>
    <?php } ?>
    </div>

   

</div>
</div>
</div>
<!--Ending of the 5 more post that can attract the users and just desinged diffrently--->

<!----------------------------------------------------------------------------------------------------------------------------------->

<!--the main part that contains all remaining post and a good sidebar for the recent post---->


<div class="container-fluid main-blog-area p-0 m-0 pt-2" style="margin-top: 8px;background:  #ffffff">

  <!---Container--->


<div class="col-sm-12 col-md-5 p-0">

<div class="row m-0 p-0">

<!--remaining post---->

  <div class="column main-column col-md-12 m-0 p-0 ml-5">
    
    <div class="card big-card col-md-11 mb-3 mt-3" style="width: 100%;margin-top: 0;margin-left: 0;border-top: 5px solid #e40c78;border: " >


      <?php

      global $conn;

      if (isset($_GET["SearchButton"])) {
       $Search=$_GET["Search"];
       $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

     }
//query when category is active

elseif (isset($_GET["Category"])) {
      $Category=$_GET["Category"];
      $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
    }

    elseif (isset($_GET["Page"])) {
      $Page=$_GET["Page"];
      if ($Page==0 ||$Page<1) {
        $ShowPostFrom=0;   
      }

      else{

        $ShowPostFrom=($Page*5)-5;}
        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 9,1";
      }

      else{
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
     }

     $Execute=mysqli_query($conn,$ViewQuery);
     while ($DataRows=mysqli_fetch_array($Execute)) {
       $PostId=$DataRows["id"];
       $DataTime=$DataRows["datetime"];
       $title=$DataRows["title"];
       $Category=$DataRows["category"];
       $Admin=$DataRows["author"];
       $Image=$DataRows["image"];
       $Post=$DataRows["post"];

       ?>

        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
        <div class="card-header" style="background-color: #000000">
          <div class="card-text">

            <h2 class="text-light pb-2 pl-1">
              <?php echo htmlentities($title); ?>
                
              </h2>
            <p >
              Published on <?php echo htmlentities($DataTime)  ?>
                
              </p>
          
           <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>

          <?php } ?>
          
          </div>
        </div>
        <div class="card-body p-0 m-0">
          <img class="img-fluid" src="Upload/<?php echo  $Image; ?>">
        </div>
      </a>
   
    <?php } ?>
  
  </div>
  <!--- finishing the big post that look dirent to others-->
  
    <!---other post that are diffrent to ohters but look some how good-->
  
  <ul class="m-0 p-0 blog_main-post" style="margin-left: 0;">

    <?php

    global $conn;

    if (isset($_GET["SearchButton"])) {
     $Search=$_GET["Search"];
     $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

   }
//query when category is active
   
elseif (isset($_GET["Category"])) {
    $Category=$_GET["Category"];
    $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
  }
  elseif (isset($_GET["Page"])) {
    $Page=$_GET["Page"];
    if ($Page==0 ||$Page<1) {
      $ShowPostFrom=0;   
    }
    else{


      $ShowPostFrom=($Page*5)-5;
    }
    $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 10,8";
  }

  else{

   $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
 }

 $Execute=mysqli_query($conn,$ViewQuery);
 while ($DataRows=mysqli_fetch_array($Execute)) {
   $PostId=$DataRows["id"];
   $DataTime=$DataRows["datetime"];
   $title=$DataRows["title"];
   $Category=$DataRows["category"];
   $Admin=$DataRows["author"];
   $Image=$DataRows["image"];
   $Post=$DataRows["post"];

   ?>

   <li class="blog_item-post col-md-12 col-sm-12 col-xs-12 m-0 p-0 mb-3" style="
">
        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
      <div class="blog_item-images col-md-3 col-sm-3 m-0 p-0">
        <img class="blog_item-image img-fluid" src="Upload/<?php echo  $Image; ?>">
      </div>
      <div class="col-md-9 col-sm-9">

      <h1 class="blog_main-header text-dark" style="font-size: 17px;">

        <?php echo htmlentities($title); ?>
          
        </h1>
      <span id="Published">
       
       Published on <?php echo htmlentities($DataTime)  ?>
         
       </span>
       <br>
      <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>
          </div>

          <?php } ?>
  </a>
</li>

<?php } ?>

</ul>


<!---------Ending of easy post----------->

<!------------------------------------------------------------------------------------------------------------------------------->
  
<!------the big post that look different to other,big than others------>

 <div class="card big-card col-md-11 mb-3" style="width: 90%;margin-top: 0;margin-left: 0;border-top: 5px solid #e40c78;border: " >


      <?php

      global $conn;

      if (isset($_GET["SearchButton"])) {
       $Search=$_GET["Search"];
       $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

     }
//query when category is active

elseif (isset($_GET["Category"])) {
      $Category=$_GET["Category"];
      $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
    }

    elseif (isset($_GET["Page"])) {
      $Page=$_GET["Page"];
      if ($Page==0 ||$Page<1) {
        $ShowPostFrom=0;   
      }

      else{

        $ShowPostFrom=($Page*5)-5;}
        $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 18,1";
      }

      else{
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
     }

     $Execute=mysqli_query($conn,$ViewQuery);
     while ($DataRows=mysqli_fetch_array($Execute)) {
       $PostId=$DataRows["id"];
       $DataTime=$DataRows["datetime"];
       $title=$DataRows["title"];
       $Category=$DataRows["category"];
       $Admin=$DataRows["author"];
       $Image=$DataRows["image"];
       $Post=$DataRows["post"];

       ?>

        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
        <div class="card-header" style="background-color: #000000">
          <div class="card-text">

            <h2 class="text-light pb-2 pl-1">
              <?php echo htmlentities($title); ?>
                
              </h2>
            <p >
              Published on <?php echo htmlentities($DataTime)  ?>
                
              </p>
          
           <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>

          <?php } ?>
          
          </div>
        </div>
        <div class="card-body p-0 m-0">
          <img class="img-fluid" src="Upload/<?php echo  $Image; ?>">
        </div>
      </a>
   
    <?php } ?>
  
  </div>

<!--- Ending of the big post-->

<!--------------------------------------------------------------------------------------------------------------------------------->

<!--- contunous other blog post that look  small and easlly-->

<ul class="blog_main-post m-0 p-0" style="margin-left: 0;">

    <?php

    global $conn;

    if (isset($_GET["SearchButton"])) {
     $Search=$_GET["Search"];
     $ViewQuery="SELECT * FROM admin_panel WHERE id LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";

   }
//query when category is active
   
elseif (isset($_GET["Category"])) {
    $Category=$_GET["Category"];
    $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc ";  
  }
  elseif (isset($_GET["Page"])) {
    $Page=$_GET["Page"];
    if ($Page==0 ||$Page<1) {
      $ShowPostFrom=0;   
    }
    else{


      $ShowPostFrom=($Page*5)-5;
    }
    $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 19,8";
  }

  else{

   $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,3";
 }

 $Execute=mysqli_query($conn,$ViewQuery);
 while ($DataRows=mysqli_fetch_array($Execute)) {
   $PostId=$DataRows["id"];
   $DataTime=$DataRows["datetime"];
   $title=$DataRows["title"];
   $Category=$DataRows["category"];
   $Admin=$DataRows["author"];
   $Image=$DataRows["image"];
   $Post=$DataRows["post"];

   ?>

   <li class="blog_item-post col-md-12 col-sm-12 m-0 p-0 mb-3">
        <a href="detaill?title=<?php echo $title; ?>" style="text-decoration: none">
      <div class="blog_item-images col-md-3 col-sm-3 m-0 p-0">
        <img class="blog_item-image img-fluid" src="Upload/<?php echo  $Image; ?>">
      </div>
      <div class="col-md-9 col-sm-9">

      <h1 class="blog_main-header text-dark" style="font-size: 17px;">

        <?php echo htmlentities($title); ?>
          
        </h1>
      <span id="Published">
       
       Published on <?php echo htmlentities($DataTime)  ?>
         
       </span>
       <br>
      <?php 
            $conn;
            $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
            $ExecuteApproved=mysqli_query($conn,$QueryApproved);
            $RowsApproved=mysqli_fetch_array($ExecuteApproved);
            $TotalApproved=array_shift($RowsApproved);
            if ( $TotalApproved>0) {


              ?>

          
            <p style=" font-family:'Times New Roman', Times, serif;">
              <?php echo $TotalApproved; ?> Comments 

            </p>
          </div>

          <?php } ?>
  </a>
</li>

<?php } ?>

</ul>

<!---Ending of the small post -->

</div>
</div>
</div>

<!-------Ending of left colomn------------------->

<!--------------------------------------------------------------------------------------------------------------------------------->

<!--the below code are for the side bar code adn contain all of the content of the side bar-->

<div class="col-md-4 bg-light mt-1 ml-5 m-0 p-0">
 <P class="p-5">ADS</P>
</div>





<div class="column aside-column col-md-2 col-sm-3 mt-1 m-0 p-0 mr-5">
<div class="row main-row">
<div class="column aside-column col-md-12 col-sm-12">

  <div class="col-sm-offset-1 col-sm-5 col-md-12 col-sm-12">
    <h3 class="c-rock-list__title col-sm-9 col-md-12" style="font-size: 19px;letter-spacing: 0px">
      Recent Post
    </h3>
    <div class="c-rock-list__body col-sm-9 col-md-12">
     
     <!------------first recent post------------->

     <span class="c-rock-list__item--body">
      
       <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
        <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none">
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3" style="font-size: 14px;">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p ><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 

  </span>

  <!------------Second recent post------------->

  <span class="c-rock-list__item--body">

  <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 1,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
        <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none">
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p ><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 

</span>

<!------------third recent post------------->

<span class="c-rock-list__item--body">

  <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 2,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
        <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none">
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p ><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 
</span>

<!------------link for more post------------->

<div class="c-rock-list__cta">
  <a href="all" class="c-rock-list__more" style="color: #e40c78;text-decoration: none;font-size: 1px;width: 240px;">
    watch more
  </a>
</div>
</div>     

</div>
</div>
</div>
</div>
</div>  

<!------------------------------------------------------Ending of main row------------------------------------------------------------>
<div class="btn_post col-md-12 col-sm-12 p-0 m-0 mt-5">
<div class="col-sm-12 col-md-12 p-0 m-0">
    <a class="look_more-post col-sm-12 col-md-12 m-0" href="all" style="text-decoration: none;background-color: #e40c78;color: #ffffff;text-align: center;padding-top: 15px;padding-bottom: 15px;font-size: 16px;letter-spacing: -1px;font-weight: bold;border: none;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;width: 100%">More Post&nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right"></i></a>
</div>
</div>
<br><br>



<!------------Ending of main blog area------------->

<!------------------------------------------------------------------------------------------------------------------------------->

<!------------starting footer------------->
<div class="col-md-12 p-0 m-0">
<footer>
  <div id="footer-area" class="m-0 pt-5 p-0">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>About WebRoom</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>follow us</h3> 
            <ul class="link-area">
              <li><a href=""><i class="fa fa-facebook"></i>albertomelviss@gmail.com</a></li>
              <li><a href=""><i class="fa fa-twitter"></i>Kigali, Rwanda</a></li>    
            </ul>     
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>Email Us</h3>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-envelope-o"></i>albertomelviss@gmail.com</a></li>
              <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>    
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h3>Our Contact</h3>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-phone"></i>+250786674794</a></li>
              <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

</footer>
</div>

<!------------------------------------------------------Ending of footer-------------------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------>


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


  <script defer src="https://use.fontawesome.com/releases/v5.0.6/is/all.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
</body>

<!------------------------------------------------------Ending of body---------------------------------------------------------------->

</html>

<!---------------------------------------------------Ending of the html and the hole project------------------------------------------>