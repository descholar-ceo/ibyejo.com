<?php 
require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php Confirm_Login(); ?>

<?php 
if (isset($_POST["Submit"])) {
    $Username=mysqli_real_escape_string($conn, $_POST["username"]);
    $Password=mysqli_real_escape_string($conn, $_POST["Password"]);
    $ConfirmPassword=mysqli_real_escape_string($conn, $_POST["ConfirmPassword"]);

    date_default_timezone_set("Africa/Kigali");
$CurrentTime=time();
$DateTime=strftime("%d-%B-%Y %H:%M:%S",$CurrentTime);
$DateTime;
$Admin=$_SESSION["Username"];

if(empty($Username) || empty($Password) || empty($ConfirmPassword)) {
    $_SESSION["ErrorMessage"]="All field must be filled out";
    Redirect_to("admins.php");

}elseif(strlen($Password)<4) {
    $_SESSION["ErrorMessage"]="Atleast 4 character for password are requires";
    Redirect_to("admins.php");

}elseif(strlen($Password!==$ConfirmPassword)) {
    $_SESSION["ErrorMessage"]="Passwords doesnt match";
    Redirect_to("admins.php");

}else{

    global $conn;
    $Query="INSERT INTO registration(datetime,username,password,addedby)
    VALUES('$DateTime','$Username','$Password','$Admin')";
    $Execute=mysqli_query($conn,$Query);
    if ($Execute) {
         $_SESSION["SuccessMessage"]="Admin Added successfully";
    Redirect_to("Admins.php");
         
    }else{
         $_SESSION["ErrorMessage"]="Admin failed to add";
    Redirect_to("Admins.php");
    }

}

}

?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Manage Admins access</title>
        

         <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="css/adminstyles.css">


                  <style>
                .FieldInfo{
                    color: rgb(251, 174, 44);
                    font-family: Bitter,Georgia,"Times New Roman",Times,serif;
                    font-size: 1.2em;
                    

                </style>




    </head>
    <body>

         <div style="height: 10px; background: #27aae1;"></div>
        <nav class="navbar navbar-inverse" role="navigation">

            <div class="container">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>


                    </button>
                 <a class="navbar-brand" href="blog.php">   
                    <img style="margin-top: -15px;" src="images/Capture7.PNG" width=80; height=50>
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="collapse">

                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="blog.php" target="_blank">Blog</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Features</a></li>
                </ul>


                <form action="blog.php" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="Search">
                    </div>
                    <button class="btn btn-default" name="SearchButton">Go</button>
                </form>
                </div>



            </div>
            
        </nav>

            <div class="Line" style="height: 10px; background: #27aae1;"></div>






        <div class="container-fluid">
            <div class="row">
                
                <div class="col-sm-2">


                    <ul id="Side-Menu" class="nav nav-pills nav-stacked">
                        <li><a href="Dashboard.php"><span class="glyphicon glyphicon-th"></span>&nbsp;Dashboard</a></li>
                         <li><a href="AddNewPost.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Add new Post</a></li>
 <li><a href="categories.php"><span class="glyphicon glyphicon-tags"></span>&nbsp;Categories</a></li>
 <li class="active"><a href="admins.php"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Admin</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-comment"></span>&nbsp;Comments</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-equalizer"></span>&nbsp;Live Blog</a></li>
 <li><a href="Logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
`

                    </ul>
                </div>




                <!-- Ending of side area -->

                <div class="col-sm-10"> 
                   <h1>Manage Admins</h1>
                   <?php echo Message(); 
                         echo SuccessMessage();

                        ?>

                    <div>
                        
                        <form action="admins.php" method="post">

                            <fieldset>
                                <div class="form-group">
                                <label for="UserName"><span class="FieldInfo">UserName:</span></label>

                                <input class="form-control" type="text" name="username" id="username" placeholder="Username">

                                </div>
                                 <div class="form-group">
                                <label for="Password"><span class="FieldInfo">Password:</span></label>
                                <input class="form-control" type="Password" name="Password" id="Password" placeholder="Password">

                                </div>
                                 <div class="form-group">
                                <label for="ConfirmPassword"><span class="FieldInfo">Confirm Password:</span></label>

                                <input class="form-control" type="Password" name="ConfirmPassword" id="ConfirmPassword" placeholder="Retype the Password">

                                </div>
                                <br>

                                <input class="btn btn-success btn-block" type="submit" name="Submit" value="Add New Admin">
                            </fieldset>   
                            </br>                         


                        </form>
                    </div> 


                <div class="table-responsive">

                <table class="table table-striped table-hover">
                    <tr>
                        <th>Sr No.</th>
                        <th>Date & Time</th>
                        <th>Admin Name</th>
                        <th>Added by</th>
                        <th>Action</th>

                    </tr>

                    <?php 
                    global $conn;
                    $ViewQuery="SELECT * FROM registration ORDER BY id desc";
                    $Execute=mysqli_query($conn,$ViewQuery);
                    $SrNo=0;
                    while ($DataRows=mysqli_fetch_array($Execute)) {
                        $Id=$DataRows["id"];
                        $DateTime=$DataRows["datetime"];
                        $Username=$DataRows["username"];
                        $Admin=$DataRows["addedby"];
                        $SrNo++;
                        
                    
                     ?>
                     <tr>
                         <td><?php echo $SrNo; ?></td>
                         <td><?php echo $DateTime; ?></td>
                         <td><?php echo $Username; ?></td>
                         <td><?php echo $Admin; ?></td>
                         <td><a href="DeleteAdmin.php?id=<?php echo $Id; ?>"><span class="btn btn-danger">Delete</span></a></td>

                     </tr>
                 <?php } ?>
                    

                </table>
                </div>
            </div>

                <!-- Ending of main area -->
            </div>

            <!-- Ending of row-->
        </div>
            <!-- Ending of container-->

<div id="footer">
    <hr>
    <p>Theme by | Gikundiro koloni | &copy;2019-2020 ---- Allright reserved.</p>
    <a style="color: white; text-decoration: none; cursor: pointer; font-weight: bold;" href="#">
        


    </a>
   
</div>




    </body>
    </html>