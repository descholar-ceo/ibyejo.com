<?php 
require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>

<?php Confirm_Login(); ?>

<?php 
if (isset($_POST["Submit"])) {
    $Title=mysqli_real_escape_string($conn, $_POST["Title"]);
    $Category=mysqli_real_escape_string($conn, $_POST["Category"]);
    $Post=mysqli_real_escape_string($conn, $_POST["Post"]);
    date_default_timezone_set("Africa/Kigali");
$CurrentTime=time();
$DateTime=strftime("%d-%B-%Y %H:%M:%S",$CurrentTime);
$DateTime;
$Admin=$_SESSION["Username"];
$Image=$_FILES["Image"]["name"];
$Target="Upload/".basename($_FILES["Image"]["name"]);
if(empty($Title)) {
    $_SESSION["ErrorMessage"]="Title can not be empty";
    Redirect_to("AddNewPost.php");

}elseif(strlen($Title)<5) {
    $_SESSION["ErrorMessage"]="Title should be atleast 5 characters";
    Redirect_to("AddNewPost.php");

}elseif(empty($Post)) {
    $_SESSION["ErrorMessage"]="Category can not be empty";
    Redirect_to("AddNewPost.php");

}else{

    global $conn;
    $Query="INSERT INTO admin_panel(datetime,title,category,author,image,post)
    VALUES('$DateTime','$Title','$Category','$Admin','$Image','$Post')";
    $Execute=mysqli_query($conn,$Query);
    move_uploaded_file($_FILES["Image"]["tmp_name"], $Target);


    if ($Execute) {
         $_SESSION["SuccessMessage"]="Post Added successfully";
    Redirect_to("AddNewPost.php");
         
    }else{
         $_SESSION["ErrorMessage"]="Something went wrong try again";
    Redirect_to("AddNewPost.php");
    }

}

}

?>


<!DOCTYPE>

<html lang="en">
    <head>
        <title>Add new Post</title>
        <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>

         <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">

        <link rel="stylesheet" href="css/bootstrap.min.css">
       
                <link rel="stylesheet" href="css/adminstyles.css">


                  <style>
                .FieldInfo{
                    color: rgb(251, 174, 44);
                    font-family: Bitter,Georgia,"Times New Roman",Times,serif;
                    font-size: 1.2em;
                    

                </style>




    </head>
    <body>
         <div style="height: 10px; background: #27aae1;"></div>
        <nav class="navbar navbar-inverse" role="navigation">

            <div class="container">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>


                    </button>
                 <a class="navbar-brand" href="blog.php">   
                    <img style="margin-top: -15px;" src="images/Capture7.PNG" width=80; height=50>
                    </a>
                </div>


                <div class="collapse navbar-collapse" id="collapse">

                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="blog.php" target="_blank">Blog</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Features</a></li>
                </ul>


                <form action="blog.php" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="Search">
                    </div>
                    <button class="btn btn-default" name="SearchButton">Go</button>
                </form>
                </div>



            </div>
            
        </nav>

            <div class="Line" style="height: 10px; background: #27aae1;"></div>


        <div class="container-fluid">
            <div class="row">
                
                <div class="col-sm-2">


                    <ul id="Side-Menu" class="nav nav-pills nav-stacked">
                        <li><a href="Dashboard.php"><span class="glyphicon glyphicon-th"></span>&nbsp;Dashboard</a></li>
                         <li   class="active"><a href="AddNewPost.php"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Add new Post</a></li>
 <li><a href="categories.php"><span class="glyphicon glyphicon-tags"></span>&nbsp;Categories</a></li>
 <li><a href="admins.php"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Admin</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-comment"></span>&nbsp;Comments</a></li>
 <li><a href="Dashboard.php"><span class="glyphicon glyphicon-equalizer"></span>&nbsp;Live Blog</a></li>
 <li><a href="Logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
`

                    </ul>
                </div>




                <!-- Ending of side area -->

                <div class="col-sm-10"> 
                   <h1>Add New Post</h1>
                   <?php echo Message(); 
                         echo SuccessMessage();

                        ?>

                    <div>
                        
                        <form action="AddNewPost.php" method="post" enctype="multipart/form-data">

                            <fieldset> 
                                <div class="form-group">
                                <label for="title"><span class="FieldInfo">Title:</span></label>

                                <input class="form-control" type="text" name="Title" id="title" placeholder="Title">

                                </div>

                                 <div class="form-group">
                                <label for="categoryselect"><span class="FieldInfo">Category:</span></label>

                                <select class="form-control" id="categoryselect" name="Category">
                                    
                                     <?php 
                    global $conn;
                    $ViewQuery="SELECT * FROM category ORDER BY datetime desc";
                    $Execute=mysqli_query($conn,$ViewQuery);
                    while ($DataRows=mysqli_fetch_array($Execute)) {
                        $Id=$DataRows["id"];
                        $CategoryName=$DataRows["name"];
                        
                    
                     ?>

                     <option><?php echo $CategoryName; ?></option>

                 <?php } ?>

                                </select>
                                </div>

                                 <div class="form-group">
                                <label for="imageselect"><span class="FieldInfo">Select Image:</span></label>

                                <input type="File" class="form-control" name="Image" id="imageselect">
                            </div>


                              <div class="form-group">
                                <label for="postarea"><span class="FieldInfo">Post:</span></label>

                                <textarea class="form-control" name="Post" id="postarea"></textarea>
                                <script>
                        CKEDITOR.replace( 'Post' );
                </script>

                            </div>

                                <br>
                            


                                <input class="btn btn-success btn-block" type="submit" name="Submit" value="Add New Post">
                            </fieldset>   
                            </br>                         


                        </form>
                    </div> 


                
                   
                    
            </div>

                <!-- Ending of main area -->
            </div>

            <!-- Ending of row-->
        </div>
            <!-- Ending of container-->

<div id="footer">
    <hr>
    <p>Theme by | Gikundiro koloni | &copy;2019-2020 ---- Allright reserved.</p>
    <a style="color: white; text-decoration: none; cursor: pointer; font-weight: bold;" href="#">
        
<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>

    </a>
   
</div>

<script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>

 <script src="js/jquery-3.5.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>

    </body>
    </html>


    