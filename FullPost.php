<?php require_once("include/DB.php"); ?>
<?php require_once("include/Sessions.php"); ?>
<?php require_once("include/functions.php"); ?>



<?php 
if (isset($_POST["Submit"])) {
  $Name=mysqli_real_escape_string($conn, $_POST["Name"]);
  $Email=mysqli_real_escape_string($conn, $_POST["Email"]);
  $Comment=mysqli_real_escape_string($conn, $_POST["Comment"]);
  date_default_timezone_set("Africa/Kigali");
  $CurrentTime=time();
  $DateTime=strftime("%d-%B-%Y %H:%M:%S",$CurrentTime);
  $DateTime;

  $PostId=$_GET['id'];


  if(empty($Name) || empty($Email) || empty($Comment)) {
    $_SESSION["ErrorMessage"]="All field are required";

  }elseif(strlen($Comment)>500) {
    $_SESSION["ErrorMessage"]="Only 500 character are allowed in a comment";

  }else{

    global $conn;
    $PostIDFromURL=$_GET['id'];
    $Query="INSERT INTO comments (datetime,name,email,comment,approvedby,status,admin_panel_id) VALUES('$DateTime','$Name','$Email','$Comment','pending','OFF',' $PostIDFromURL')";
    $Execute=mysqli_query($conn,$Query);

    if ($Execute) {
     $_SESSION["SuccessMessage"]="Comments submitted successfully";
     Redirect_to("FullPost.php?id={$PostId}");

   }else{
     $_SESSION["ErrorMessage"]="Something went wrong try again";
     Redirect_to("FullPost.php?id={$PostId} ?>");
   }

 }

}

?>




<!DOCTYPE html>
<html lang="en">
<head>
  <title>Full new</title>

   <meta charset="UTF-8">
  <meta name="author" content="Niyigena Alberto">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="tech we news">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript,php">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/fullPost.css">

  <link rel="stylesheet" href="css/blogstyle.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


  <style>
  
  


  @media only screen and (max-width: 910px){ 
  body{
    font-size: 16px;
  }  
.aside-column{
display: none;
    }
    .container .row .main-column .full-text .package{

      display: none;
    }
    .main-column{
      width: 100% !important;
      background-color: red;
    }
    .post{ 
      width: 97% !important;
      margin-right: 5px !important;
            font-size: 16px !important;

    }
  section .container .navbar{
width: 100%;
font-size: 25px;
}
.search-form{
  display: none;
}
#newsLetter li a{
  font-size: 15px;
}
 .form-control{
  font-size: 16px;
  }
  .btn{
    font-size: 20px;
  }


}
  @media only screen and (min-width: 911px) and (max-width: 1342px){ 
   
.aside-column{
    }
     .container .row .main-column .full-text .package{

      display: none;
    }
     .main-column{
      width: 55% !important;
    }
     .post{ 
      width: 98% !important;
      margin-right: 5px !important;
            font-size: 16px !important;

    }

    .aside-column{
      margin-right: 25px;
    }
        .col-sm-offset-1{
      width: 100% !important;
      margin-left: -16px !important; 
}
.c-rock-list__item--body .pull-left{
  width: 93% !important;
}
.sec-para-l{
  width: 95% !important;
  font-size: 15px !important;
}
.c-rock-list__cta{
  font-size: 25px;
}
  }



 @media only screen and (min-width: 1343){ 
  body{
    font-size: 25px;
  }  
   .package__fTJxTFE5iG{
display: none;  }
.aside-column{
display: none;
    }
    .container .row .main-column .full-text .package{

      display: none;
    }
    .main-column{
      width: 100% !important;
      background-color: red;
    }
    .post{ 
      width: 97% !important;
      margin-right: 5px !important;
            font-size: 16px !important;

    }
  section .container .navbar{
width: 100%;
font-size: 25px;
}
.search-form{
  display: none;
}
#newsLetter li a{
  font-size: 20px;
}
 .form-control{
  font-size: 30px;
  }
  .btn{
    font-size: 30px;
  }


}


@media only screen and (min-width: 1343px){ 

   section .container .navbar{
width: 60%;
}
#newsLetter li a{
  font-size: 15px;
}
.post{ 
      width: 95% !important;
      margin-right: 5px !important;
      font-size: 16px !important;
    }
    .col-sm-offset-1{
      width: 80% !important;
      margin-left: 86px !important;
}
.c-rock-list__item--body .pull-left{
  width: 93% !important;
}
.sec-para-l{
  width: 95% !important;
  font-size: 25px;
}
.c-rock-list__cta{
  font-size: 25px;
}


  }

</style>


</head>
<body>





  <!--the begining of the main header that contains all buttons that direct you to the different pages-->
  <nav class="navbar navbar-expand-lg navbar-light m-0 p-0" style="background-color: #000000;border: none;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto" style="text-transform: uppercase;  letter-spacing: -1px;font: small-caps bold 13px/30px Georgia, serif;
">
      <li class="nav-item active">
        <a class="nav-link text-light" href="ibyejo">Home</a>
      </li>
       <?php
      global $conn;
      $ViewQuery="SELECT * FROM category ORDER BY id desc";
      $Execute=mysqli_query($conn,$ViewQuery);
      while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows['id'];
        $Category=$DataRows['name'];
        ?>

      <li class="nav-item" style="font-size: 14px">
        <a class="nav-link text-light" href="category.php?Category=<?php
        echo $Category; ?>"><?php echo $Category."<br>"; ?></a>
      </li>
     <?php }?>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="search">
      <input class="form-control mr-sm-2" name="Search" type="search" placeholder="Search" aria-label="Search" style="-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">
      <button name="SearchButton" class="btn btn-outline-success my-2 my-sm-0" type="submit" style="border: none;background-color: #e40c78;color: #ffffff;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">Search</button>
    </form>
  </div>
</nav>

<!--the main part that contains all remaining post and a good sidebar for the recent post---->

<div class="container-fluid p-0 m-0 pt-2" style="margin-top: 8px;background-color: #ffffff">
  <div class="col-sm-8" >
      <div class="column main-column col-md-11" style="background-color: #ffffff" >
              <div class="full-text">
                 <?php echo Message(); 
        echo SuccessMessage();

        ?>

         <?php

        global $conn;

        if (isset($_GET["SearchButton"])) {
         $Search=$_GET["Search"];
         $ViewQuery="SELECT * FROM admin_panel WHERE datetime LIKE '%$Search%' OR title LIKE '%$Search%' OR category LIKE '%$Search%' OR post LIKE '%$Search%'";
       }else{
        $POSTIDFromURL=$_GET["title"];


        $ViewQuery="SELECT * FROM admin_panel WHERE title='$POSTIDFromURL' ORDER BY datetime desc";}

        $Execute=mysqli_query($conn,$ViewQuery);
        while ($DataRows=mysqli_fetch_array($Execute)) {
         $PostId=$DataRows["id"];
         $DataTime=$DataRows["datetime"];
         $title=$DataRows["title"];
         $Category=$DataRows["category"];
         $Admin=$DataRows["author"];
         $Image=$DataRows["image"];
         $Post=$DataRows["post"];

         ?>


       <?php } ?>

       <div class="col-sm-10 col-md-12">
        <h1 style="font-size: 30px;"><?php echo htmlentities($title); ?></h1>
        <br>
        <p><?php  echo htmlentities($Category);?>| <b style="color: #e40c78">Posted on:</b> <?php echo htmlentities($DataTime)  ?></p>  
      </div>

      <br>
      <div class="icon col-md-5 p-0 m-0 ml-2">
        <i class="fa fa-facebook-f facebook" style="color:#e40c78;"></i>&nbsp;
        <i class="fa fa-instagram" style="color:#e40c78;"></i>&nbsp;
        <i class="fa fa-twitter" style="color:#e40c78;"></i>
      </div>



      <img class="img-fluid" src="Upload/<?php echo  $Image; ?>" style="border: none;-moz-border-radius: 0px;
-webkit-border-radius: 0px;
border-radius: 0px;">

      <div class="caption">
        <p class="post" ><?php echo  nl2br ($Post); ?></p>
      </div>



      <div style="" class="package package__fTJxTFE5iG default__yn_LCxrffG col-sm-12 col-md-12" role="region" aria-label="Shopping <span aria-hidden=&quot;true&quot;>🛍</span>">


        <h4 class="Recomended">
          <?php echo $Category ; ?>
          &nbsp;Recomended
          <span class="glyphicon glyphicon-fire"></span>
        </h4>

        <?php
        global $conn;
        $ViewQuery="SELECT * FROM admin_panel WHERE category='$Category' ORDER BY id desc LIMIT 0,3";
        $Execute=mysqli_query($conn,$ViewQuery);
        while ($DataRows=mysqli_fetch_array($Execute)) {
          $Id=$DataRows["id"];
          $Title=$DataRows["title"];
          $DateTime=$DataRows["datetime"];
          $Image=$DataRows["image"];
          if (strlen($DateTime)>11) {
            $DateTime=substr($DateTime, 0,11);
          }
          ?>


          <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none;">

            <div class="card col-sm-4 col-md-4 p-0" style="border: none;">

              <div class="card-header p-0 m-0">
                <img class="img-fluid p-0 m-0 pl-1" role="img" src="Upload/<?php echo htmlentities($Image) ?>" style="max-height: 120px;width: 100%; object-fit: cover;">

              </div>


              <div class="card-body col-sm-12 col-md-12 p-0 m-0 pl-1 pr-1">


                <span class="text-dark" style="text-transform: lowercase;">
                  <?php
                  if(strlen($Title)>45){$Title=substr($Title,0,45).'..';}

                  echo htmlentities($Title); ?>


                </span>


                <p class="description" style="color: #e40c78"><?php echo htmlentities($DateTime); ?></p>

              </div>
            </div>
          </a>
        <?php } ?>          
      </div>


      <!----comment block---->
 <?php 
      $conn;
      $QueryApproved="SELECT COUNT(*) FROM comments WHERE admin_panel_id='$PostId' AND status='ON'";
      $ExecuteApproved=mysqli_query($conn,$QueryApproved);
      $RowsApproved=mysqli_fetch_array($ExecuteApproved);
      $TotalApproved=array_shift($RowsApproved);
      if ( $TotalApproved>0) {

        ?>

        <span class="FieldInfo" style="margin-left: 15px;
        ">
        <?php echo $TotalApproved; ?>
        &nbsp;Comments
      <?php } ?>

    </span>
    <br>
    <br>
    <?php
    $conn;
    $PostIdForComments=$_GET["id"];

    $ExtractingQuery="SELECT * FROM comments 
    WHERE admin_panel_id='$PostIdForComments' AND status='ON' ";
    $Execute=mysqli_query($conn,$ExtractingQuery);
    while ($DataRows=mysqli_fetch_array($Execute)) {

      $CommentDate=$DataRows["datetime"];
      $CommenterName=$DataRows["name"];
      $CommentbyUsers=$DataRows["comment"];




      ?>
      <div class="commentBlock" style="">
        <img class="pull-left" src="images/comment.PNG" width="70px" height="80px;">

        <p class="Comment-info p-0 mb-0">
          <?php echo  $CommenterName; ?>

        </p>

        <p class="description p-0 mb-0">
          <?php echo  $CommentDate; ?>

        </p>

        <br>
        <p class="comment mt-0">
          <?php echo  nl2br($CommentbyUsers); ?></p>
        </div>

        <hr  style="margin-left: 15px;">

      <?php } ?>

      <br>
      <span class="FieldInfo" style="margin-left: 15px;"> share your thoughts about this post</span>

      <!------user to comment---->


            <div class="user-comment">
               <form action="FullPost.php?id=<?php echo $PostId; ?>" method="post" enctype="multipart/form-data">

                 <fieldset> 

            <div class="form-group">
              <label for="Name">
                <span class="FieldInfo">Name:</span>
              </label>

              <input class="form-control" type="text" name="Name" id="Name" placeholder="Name">

            </div>

            <div class="form-group">
              <label for="Email">
                <span class="FieldInfo">Email:</span>
              </label>

              <input class="form-control" type="Email" name="Email" id="Email" placeholder="Email">

            </div>

            <div class="form-group">
              <label for="commentarea">
                <span class="FieldInfo">Comments:</span>
              </label>

              <textarea class="form-control" name="Comment" id="commentarea"></textarea>

            </div>

            <br>



            <input class="btn" type="submit" name="Submit" value="Submit" style="">
          </fieldset>  
                </form>
 


            </div>








  </div>


   
  </div>
  </div>
  <div class="col-sm-4 col-md-4">
   <div class="row main-row">
<div class="column aside-column col-md-10 col-sm-12">

  <div class="col-sm-offset-1 col-sm-5 col-md-12 col-sm-12">
    <h3 class="c-rock-list__title col-sm-9 col-md-12" style="font-size: 19px;letter-spacing: 0px">
      Recent Post
    </h3>
    <div class="c-rock-list__body col-sm-9 col-md-12">
     
     <!------------first recent post------------->

     <span class="c-rock-list__item--body">
      
       <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 0,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
          <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none;">
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3" style="font-size: 14px;">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p style="color: #e40c78;"><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 

  </span>

  <!------------Second recent post------------->

  <span class="c-rock-list__item--body">

  <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 1,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
          <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none;">
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p style="color: #e40c78;"><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 

</span>

<!------------third recent post------------->

<span class="c-rock-list__item--body">

  <?php
       global $conn;
       $ViewQuery="SELECT * FROM admin_panel ORDER BY id desc LIMIT 2,1";
       $Execute=mysqli_query($conn,$ViewQuery);
       while ($DataRows=mysqli_fetch_array($Execute)) {
        $Id=$DataRows["id"];
        $Title=$DataRows["title"];
        $DateTime=$DataRows["datetime"];
        $Image=$DataRows["image"];
        if (strlen($DateTime)>16) {
          $DateTime=substr($DateTime, 0,16);
        }
        ?>
        <div class="col-sm-12 col-md-12 p-0 m-0">
          <a href="detaill?title=<?php echo $Title; ?>" style="text-decoration: none;">
          <div class="col-sm-12 col-md-12 p-0 m-0">
           <img class="img-fluid pt-3" style="width: 100%" src="Upload/<?php echo htmlentities($Image) ?>" width=100;height=90;>
         </div>
 <div class="col-sm-12 col-md-12 p-0 m-0">
           <p class="text-dark pt-3">
            <?php
            if(strlen($Title)>40){$Title=substr($Title,0,40).'....';}

            echo htmlentities($Title); ?>


          </p>

          <p style="color: #e40c78;"><?php echo htmlentities($DateTime); ?></p>
        </div>

        </a>
      </div>

    <?php } ?> 
</span>

<!------------link for more post------------->

<div class="c-rock-list__cta">
  <a href="all" class="c-rock-list__more" style="color: #e40c78;text-decoration: none;font-size: 1px;width: 240px;">
    watch more
  </a>
</div>
</div>     

</div>
</div>
</div>
  </div>

</div>

<div class="col-md-12 p-0 m-0 mt-2">
<footer>
  <div id="footer-area" class="m-0 pt-5 p-0">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h5>About WebRoom</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h5>follow us</h5> 
            <ul class="link-area">
              <li><a href=""><i class="fa fa-facebook"></i>albertomelviss@gmail.com</a></li>
              <li><a href=""><i class="fa fa-twitter"></i>Kigali, Rwanda</a></li>    
            </ul>     
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h5>Email Us</h5>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-envelope-o"></i>albertomelviss@gmail.com</a></li>
              <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>    
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-footer">
            <h5>Our Contact</h5>
            <ul class="link-area">
              <li><a href=""><i class="fa fa-phone"></i>+250786674794</a></li>
              <li><a href=""><i class="fa fa-map"></i>Kigali, Rwanda</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

</footer>
</div>




<!------------------------------------------------------Ending of footer-------------------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------>


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script defer src="https://use.fontawesome.com/releases/v5.0.6/is/all.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="js/jquery-3.5.0.min.js"></script>
  <script src="js/bootstrap.min.js"></script>






<script src="js/jquery-3.5.0.min.js"></script>

<script src="js/bootstrap.min.js"></script>


</body>
</html>